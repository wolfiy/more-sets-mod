package net.moresetsmod;

import net.fabricmc.api.ModInitializer;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.moresetsmod.armor.ModArmorMaterials;
import net.moresetsmod.tools.AxeBase;
import net.moresetsmod.tools.HoeBase;
import net.moresetsmod.tools.PickaxeBase;
import net.moresetsmod.tools.ShovelBase;
import net.moresetsmod.tools.SwordBase;
import net.moresetsmod.tools.ToolMaterialObsidian;

public class MoreSetsMod implements ModInitializer {

    private static final Item OBSIDIAN_INGOT = new ObsidianIngot(new Item.Settings().group(ItemGroup.MATERIALS));

    public static final Item OBSIDIAN_HELMET = new ArmorItem(ModArmorMaterials.OBSIDIAN, EquipmentSlot.HEAD, (new Item.Settings().group(ItemGroup.COMBAT)));
    public static final Item OBSIDIAN_CHESTPLATE = new ArmorItem(ModArmorMaterials.OBSIDIAN, EquipmentSlot.CHEST, (new Item.Settings().group(ItemGroup.COMBAT)));
    public static final Item OBSIDIAN_LEGGINGS = new ArmorItem(ModArmorMaterials.OBSIDIAN, EquipmentSlot.LEGS, (new Item.Settings().group(ItemGroup.COMBAT)));
    public static final Item OBSIDIAN_BOOTS = new ArmorItem(ModArmorMaterials.OBSIDIAN, EquipmentSlot.FEET, (new Item.Settings().group(ItemGroup.COMBAT)));

    @Override
    public void onInitialize() {
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_ingot"), OBSIDIAN_INGOT);
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_helmet"), OBSIDIAN_HELMET);
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_chestplate"), OBSIDIAN_CHESTPLATE);
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_leggings"), OBSIDIAN_LEGGINGS);
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_boots"), OBSIDIAN_BOOTS);

        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_pickaxe"), new PickaxeBase(new ToolMaterialObsidian()));
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_axe"), new AxeBase(new ToolMaterialObsidian()));
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_hoe"), new HoeBase(new ToolMaterialObsidian()));
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_shovel"), new ShovelBase(new ToolMaterialObsidian()));
        Registry.register(Registry.ITEM, new Identifier("moresetsmod", "obsidian_sword"), new SwordBase(new ToolMaterialObsidian()));
    }
}